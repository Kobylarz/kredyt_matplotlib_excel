import shutil
import os
import logging
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.widgets import Cursor
import tempfile
import xlrd


class DataAcquire(object):
    temp_dir = tempfile.mkdtemp()
    logger = logging.getLogger()

    def __init__(self):
        pass

    def get_excel_data(self, filepath):
        self._copy_excel_file_to_temp(filepath)
        book = xlrd.open_workbook(filepath)
        for sheet in book.sheet_names():
            if sheet != 'dane':
                data = pd.read_excel(filepath, sheetname=sheet)
                yield data, sheet

    def get_plottable_data(self, data, selections,label=""):
        x_data = data['dlugosc']
        y_data = data[selections]

        results = []
        for column in y_data.columns.values:
            plot_label = "{} {}".format(label, column)
            x = x_data.tolist()
            y = y_data[column].tolist()
            item = {'label': plot_label, 'data': (x, y)}
            results.append(item)

        return results

    def _copy_excel_file_to_temp(self, filepath):
        source = filepath
        destination = os.path.join(self.temp_dir, os.path.basename(source))
        self.logger.debug("Excel copy path is: {}".format(destination))
        shutil.copy(source, destination)


excel = DataAcquire()
for bank in excel.get_excel_data(r"kredyt.xlsm"):
    bank_data, bank_label = bank

    data = excel.get_plottable_data(bank_data, selections=['malejace', 'rowne'], label=bank_label)

    fig1 = plt.figure(1)
    ax1 = fig1.add_subplot(111)
    ax1.set_title('koszt kredytu vs czas splaty')

    for item in data:
        x, y = item['data']
        ax1.plot(x, y, '.-',label=item['label'])

    data = excel.get_plottable_data(bank_data, selections=['rata rowne', 'max rata malejace'], label=bank_label)

    fig2 = plt.figure(2)
    ax2 = fig2.add_subplot(111)
    ax2.set_title('wysokosc raty vs czas splaty')
    for item in data:
        x, y = item['data']
        ax2.plot(x, y, '.-',label=item['label'])


ax1.legend()
ax1.grid()

ax2.legend()
ax2.grid()
# cursor = Cursor(ax1, useblit=True, color='red', linewidth=2)


plt.show()

